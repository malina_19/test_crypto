// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Donation {
    address payable public owner;

    constructor() {
        owner = payable(msg.sender);
    }

    struct Donators {
        // string name;
        address add;
        uint256 sum;
    }

    Donators[] public donators;

    modifier is_owner() {
        require(msg.sender == owner);
        _;
    }

    // mapping(uint256 => Donators) public donators;
    uint256 private donator_count = 0;

    event donationEvent(address indexed donator, uint256 amount);

    event transferEvent(address _from, address _to, uint256 amount);

    // function add_donate_address(address payable _to) public isOwner {
    //     donate_address = _to;
    // }

    function donate() public payable {
        // bool sent = donate_address.send(msg.value);
        // require(sent, "Failed to send Ether");
        require(msg.value >= .001 ether);
        emit donationEvent(msg.sender, msg.value);
        // for (uint256 i = 0; i <= donator_count; i++) {
        //     if (donators[i].add == msg.sender) {
        //         donators[i].sum += msg.value;
        //         break;
        //     } else {
        donators.push(Donators(msg.sender, msg.value));
        donator_count++;
        //     }
        // }
    }

    function transfer_to_owner() public payable {
        require(msg.sender == owner);
        emit transferEvent(msg.sender, owner, address(this).balance);
        owner.transfer(address(this).balance);
    }
}
