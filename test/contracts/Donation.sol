//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract Donation {
    address payable public owner;

    constructor() {
        owner = payable(msg.sender);
    }

    struct Donators {
        address add;
        uint256 sum;
    }

    Donators[] public donators;

    // mapping(uint256 => Donators) public donators;
    uint256 private donator_count = 0;

    event donationEvent(address indexed donator, uint256 amount);

    event transferEvent(address _from, address _to, uint256 amount);

    // function add_donate_address(address payable _to) public isOwner {
    //     donate_address = _to;
    // }

    function donate() public payable {
        require(msg.value >= .001 ether);
        emit donationEvent(msg.sender, msg.value);
        donators.push(Donators(msg.sender, msg.value));
        donator_count++;
    }

    function transfer_to_owner() public payable {
        require(msg.sender == owner);
        emit transferEvent(msg.sender, owner, address(this).balance);
        owner.transfer(address(this).balance);
    }

    function owner_balance() external view returns (uint256) {
        return owner.balance;
    }

    function balance() external view returns (uint256) {
        return address(this).balance;
    }

}
