const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Donation", function () {
    let owner;
    let addr1;
    let addr2;
    let addrs;
    beforeEach(async function () {
      Donation = await ethers.getContractFactory("Donation");
      [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

      hardhatDonation = await Donation.deploy();
    });

    it("Should set the right owner", async function () {
      expect(await hardhatDonation.owner()).to.equal(owner.address);
    });

    it("Should assign the total supply of tokens to the owner", async function () {
      const initialBalance = await hardhatDonation.balance();
      const initialOwnerBalance = await hardhatDonation.owner_balance();
      await hardhatDonation.transfer_to_owner();
      const ownerBalance = await hardhatDonation.owner_balance();
      expect(initialBalance == ownerBalance - initialOwnerBalance);
    });

    it("Should update balances after transfers", async function () {
      const initialBalance = await hardhatDonation.balance();

      // Transfer 5 tokens
      // await hardhatDonation.connect(addr1).donate();

      // Transfer another 10 tokens
      // await hardhatDonation.connect(addr2).donate();

      // Check balances
      const finalOwnerBalance = await hardhatDonation.balance();
      expect(finalOwnerBalance).to.equal(0);

      });
});
